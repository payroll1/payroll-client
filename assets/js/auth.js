
function clearAuthentication() {
    localStorage.removeItem(AUTH_TOKEN);
    localStorage.removeItem(AUTH_USERNAME);
    localStorage.removeItem(AUTH_USERID);
}

function storeAuthentication(session) {
    localStorage.setItem(AUTH_TOKEN, session.token);
    localStorage.setItem(AUTH_USERNAME, session.username);
    localStorage.setItem(AUTH_USERID, session.id);
}

function getUsername() {
    return localStorage.getItem(AUTH_USERNAME);
}

function getUser() {
    return localStorage.getItem(AUTH_USERID);
}

function addUser() {
    return localStorage.getItem(AUTH_USERID);
}

function getToken() {
    return localStorage.getItem(AUTH_TOKEN);
}


