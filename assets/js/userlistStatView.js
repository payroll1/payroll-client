function generateuserlistStatistic(payroll) {

    let rows = "";
    for (let i = 1; i < payroll.length; i++) {
        rows += `
       
        ${generateUserlistRowHeadingNew(payroll[i]
        )}               
        ${generateUserlistRowElementNew(
            'Töötunnid kokku:', payroll[i].workHours,
            'Keskmine töötamine päevas:', (payroll[i].workHours / payroll[i].calendar.length).toFixed(2),
            'Töötamiste arv', payroll[i].calendar.length
        )}  
        ${generateUserlistWorktime(payroll[i])}   

    `;
    }
    document.getElementById("userlistStatistic").innerHTML = /*html*/`  
    <div class="row justify-content-center">
        <div class="col-lg-10">
            ${rows}            
   <a href="#top"><strong>^</strong> Liigu ülesse</a><br>
   <button class="btn btn-danger btn-block" onClick="deleteAllFromCalendarButton()">Kustuta kõik kalendri kanded</button><br>  
   
        </div>
    </div>
`;
}

function generateUserlistWorktime(payroll) {
    if (payroll.calendar.length == 0) {

        let UserCaledarItemsHtml0 = "Töötamised puuduvad: <br>";
        for (let i = 0; i < payroll.calendar.length; i++) {
            UserCaledarItemsHtml0 = UserCaledarItemsHtml0 + `
        <div class="row">
            <div class="col-12" style="padding: 10px;">               
            <strong style="font-size: 20px;">${payroll.calendar[i].date}, töötundide arv: ${payroll.calendar[i].workHours}</strong>
            </div>   
            </div>                
    `;
        } return UserCaledarItemsHtml0;

    } else {
        let UserCaledarItemsHtml = "Töötamised:<br>";
        for (let i = 0; i < payroll.calendar.length; i++) {
            UserCaledarItemsHtml = UserCaledarItemsHtml + `
        <div class="row">
            <div class="col-12" style="padding: 10px;">               
            <strong style="font-size: 20px;">${payroll.calendar[i].date}, töötundide arv: ${payroll.calendar[i].workHours}</strong>
            </div>  
            </div>                
    `;
        } return UserCaledarItemsHtml;

    }
    
}

function generateUserlistRowHeadingNew(userlist) {
    return /*html*/`
    <div class="row">
        <div class="col-12" style="padding: 10px;">
        <i>. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</i> </div>
        <div class="col-12" style="padding: 10px;">
            <strong style="font-size: 28px;">${userlist.name}</strong> 
        </div>
    </div>      
   
`;
}

function generateUserlistRowHeadingNewNew() {
    return /*html*/`
    <div class="row">
        <div class="col-12" style="padding: 10px;">
            <style="font-size: 20px;">########################</style> 
        </div>
    </div>      
    
`;
}


function generateUserlistRowElementNew(cell1Content, cell2Content, cell3Content, cell4Content, cell5Content, cell6Content) {
    return /*html*/`
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                ${generateUserlistRowTitleCell(cell1Content)}
                ${generateUserlistRowValueCell(cell2Content)}
                ${generateUserlistRowTitleCell(cell3Content)}    
                ${generateUserlistRowValueCell(cell4Content)}  
                ${generateUserlistRowTitleCell(cell5Content)}    
                ${generateUserlistRowValueCell(cell6Content)}  
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
      
            </div>
        </div>
    </div>
`;
}