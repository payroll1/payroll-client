function generateUserlistList(payroll) {
    document.getElementById("viewSelect").style.display = "block";
    let rows = "";
    for (let i = 1; i < payroll.length; i++) {
        rows += `
            ${generateUserlistRowHeadingAdmin(payroll[i])}
            ${generateUserlistRowElement(
            'Lepingu algus:', payroll[i].birthDate != null ? payroll[i].birthDate : 'info puudub',
            'Tunnitasu (EUR)', payroll[i].payPerHour > 0 ? formatNumber(payroll[i].payPerHour) : 'info puudub'
        )}
            ${generateUserlistRowElement(
            'Töötunnid:', payroll[i].workHours > 0 ? formatNumber(payroll[i].workHours) : '0',
            'Kokku (EUR):', payroll[i].gross_income !== 0 ? formatNumber(payroll[i].gross_income) : '0'
        )}  
            ${generateUserlistRowElement(
            'Töötamiste arv:', payroll[i].calendar.length != null ? payroll[i].calendar.length : 'töötamised puuduvad',
            'Osakond:', payroll[i].extra != null ? payroll[i].extra : 'info puudub'
        )}
            ${generateUserlistRowButtons(payroll[i])}
            ${generateUserlistRowButtonsNew(payroll[i])}
        `;
    }

    document.getElementById("userlistList").innerHTML = /*html*/`
        <div class="row justify-content-center">
            <div class="col-lg-10">
                ${rows}
                <div class="row">
                <div class="col-sm-3" style="padding: 10px;">
                    <button class="btn btn-success btn-block" onClick="openUserlistModalForInsert()">Lisa töötaja</button>
                    <br>                    
                    
                    <br>
                    <a href="#top"><strong>^</strong> Liigu ülesse</a> <br>
                    </div>
                </div>
            </div>        
    `;
}

function generateEmployeeUserlistList(payroll) {
    document.getElementById("viewSelect").style.display = "none";
    let rows = "";
    let i = payroll.findIndex(p => p.id === parseInt(getUser()));
    rows += `
            ${generateUserlistRowHeading(payroll[i])}
            ${generateUserlistRowElement(
        'Lepingu algus:', payroll[i].birthDate != null ? payroll[i].birthDate : 'info puudub',
        'Tunnitasu (EUR)', payroll[i].payPerHour > 0 ? formatNumber(payroll[i].payPerHour) : 'info puudub'
    )}
            ${generateUserlistRowElement(
        'Töötunnid:', payroll[i].workHours > 0 ? formatNumber(payroll[i].workHours) : '0',
        'Kokku (EUR):', payroll[i].gross_income !== 0 ? formatNumber(payroll[i].gross_income) : '0'
    )}  
            ${generateUserlistRowElement(
        'Töötamiste arv:', payroll[i].calendar.length != null ? payroll[i].calendar.length : 'töötamised puuduvad',
        'Osakond:', payroll[i].extra != null ? payroll[i].extra : 'info puudub'
    )}
            ${generateEmployeeRowButton(payroll[i])}
            <h2;>Minu töötamised</h2>
            ${generateUserlistWorktimeUser(payroll[i])}         
        `;

    document.getElementById("userlistList").innerHTML = /*html*/`
        <div class="row justify-content-center">
            <div class="col-lg-10">
                ${rows}   
                <center>             
                <div class="row justify-content-center">
                    <div class="col-sm-3" style="padding: 10px;">
                    
                    </div>
                </div>
            </div>
        </div></center>
    `;
}

function generateUserlistRowHeading(userlist) {
    return /*html*/`
        <div class="row">
            <div class="col-12" style="padding: 10px;">
            <strong style="font-size: 28px">${userlist.name}, töötaja id: ${userlist.id}</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                ${generateImageElement(userlist.logo)}
                
            </div>
        </div>
    `;
}

function generateUserlistRowHeadingAdmin(userlist) {
    return /*html*/`
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <strong style="font-size: 28px">${userlist.name}, töötaja id: ${userlist.id}</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                ${generateImageElement(userlist.logo)}
                
            </div>
        </div>
    `;
}

function generateUserlistWorktimeUser(payroll) {
    console.log(payroll);

    let CaledarItemsHtmlUser = "";
    for (let i = 0; i < payroll.calendar.length; i++) {
        CaledarItemsHtmlUser = CaledarItemsHtmlUser + `
            <div class="row">
                <div class="col-12" style="padding: 10px;">                    
                    <strong style="font-size: 28px;">${payroll.calendar[i].date}, töötundide arv: ${payroll.calendar[i].workHours}</strong>
                </div>
            </div>        
        `;
    }
    return CaledarItemsHtmlUser;
}


function generateUserlistRowElement(cell1Content, cell2Content, cell3Content, cell4Content) {
    return /*html*/`
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    ${generateUserlistRowTitleCell(cell1Content)}
                    ${generateUserlistRowValueCell(cell2Content)}
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    ${generateUserlistRowTitleCell(cell3Content)}
                    ${generateUserlistRowValueCell(cell4Content)}
                </div>
            </div>
        </div>
    `;
}

function generateUserlistRowTitleCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateUserlistRowValueCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                ${text}
            </div>
        </div>
    `;
}

function generateUserlistRowButtons(userlist) {
    return /*html*/`
        <div class="row justify-content-center">          
        <div class="col-sm-3" style="padding: 10px;">
            <button class="btn btn-primary btn-block" onClick="openUserlistModalForUpdate(${userlist.id})">Muuda töötaja andmeid</button>
            </div>
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-danger btn-block" onClick="removeUserlist(${userlist.id}); removeUser(${userlist.id})">Eemalda töötajate nimekirjast</button>
            </div>
            <div class="col-sm-3" style="padding: 10px;">
            <button class="btn btn-success btn-block" onClick="openRegisterUserModalEdit(${userlist.id})">Halda töötaja kontot</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <hr/>
            </div>
        </div>
    `;
}

function generateUserlistRowButtonsNew(userlist) {
    return /*html*/`
        <div class="row justify-content-center">   
            <div class="col-sm-3" style="padding: 10px;">
                
            </div>
            </div>
            `;
}


function generateEmployeeRowButton(userlist) {
    return /*html*/`
        <div class="row justify-content-center">
        <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-danger btn-block" onClick="removeLastWorktimeEntry(${userlist.id})">Kustuta viimane kanne</button>                
            </div>   
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-primary btn-block" onClick="openWorktimeModalForInsert(${userlist.id})">Lisa töötunnid</button>
            </div>   
        </div>
    `;
}

function generateImageElement(logo) {
    if (!isEmpty(logo)) {
        return /*html*/`<img src="${logo}" width="150" />`;
    } else {
        return "[PILT PUUDUB]";
    }
}

function openRegisterUserModalEditShow() {
    $("#registerUserModalEdit").modal('show');
}

function closeRegisterUserModalEditHide() {
    $("#registerUserModalEdit").modal('hide');
}

function openRegisterUserModalShow() {
    $("#registerUserModal").modal('show');
}

function closeRegisterUserModalHide() {
    $("#registerUserModal").modal('hide');
}

function openUserlistModal() {
    $("#userlistModal").modal('show');
}

function closeUserlistModal() {
    $("#userlistModal").modal('hide');
}

function openWorktimeModal() {
    $("#worktimeModal").modal('show');
}

function closeWorktimeModal() {
    console.log('sulge');
    $("#worktimeModal").modal('hide');
    console.log('kas sulges?');
}

function clearErrorRegisterUser() {
    document.getElementById("errorPanelRegisterUser").innerHTML = "";
    document.getElementById("errorPanelRegisterUser").style.display = "none";
}

function showErrorRegisterUser(message) {
    document.getElementById("errorPanelRegisterUser").innerHTML = message;
    document.getElementById("errorPanelRegisterUser").style.display = "block";
}

function clearErrorUserEdit() {
    document.getElementById("errorPanelUserEdit").innerHTML = "";
    document.getElementById("errorPanelUserEdit").style.display = "none";
}

function showErrorUserEdit(message) {
    document.getElementById("errorPanelUserEdit").innerHTML = message;
    document.getElementById("errorPanelUserEdit").style.display = "block";
}

function clearErrorworktime() {
    document.getElementById("errorPanelworktime").innerHTML = "";
    document.getElementById("errorPanelworktime").style.display = "none";
}

function showErrorworktime(message) {
    document.getElementById("errorPanelworktime").innerHTML = message;
    document.getElementById("errorPanelworktime").style.display = "block";
}


function clearError() {
    document.getElementById("errorPanel").innerHTML = "";
    document.getElementById("errorPanel").style.display = "none";
}

function showError(message) {
    document.getElementById("errorPanel").innerHTML = message;
    document.getElementById("errorPanel").style.display = "block";
}

function clearUserlistModal() {
    document.getElementById("id").value = null;
    document.getElementById("name").value = null;
    document.getElementById("logo").value = null;
    document.getElementById("birthDate").value = null;
    document.getElementById("payPerHour").value = null;
    document.getElementById("workHours").value = null;
    document.getElementById("gross_income").value = null;
    //document.getElementById("net_income").value = null;
    document.getElementById("extra").value = null;
}

function fillUserlistModal(userlist) {
    document.getElementById("id").value = userlist.id;
    document.getElementById("name").value = userlist.name;
    document.getElementById("logo").value = userlist.logo;
    document.getElementById("birthDate").value = userlist.birthDate;
    document.getElementById("payPerHour").value = userlist.payPerHour;
    document.getElementById("workHours").value = userlist.workHours;
    document.getElementById("gross_income").value = userlist.gross_income;
    //document.getElementById("net_income").value = userlist.net_income;
    document.getElementById("extra").value = userlist.extra;

}
function fillUserlistModalLogoField(logo) {
    document.getElementById("logo").value = logo;

}

function clearWorktimeModal() {
    document.getElementById("worktimeModaldate").value = null;
    document.getElementById("worktimeModalWorkHours").value = null;
}

function fillRegisterUserModalEdit(user) {
    document.getElementById("editusername").value = user.username;
    document.getElementById("editpassword").value = user.password;
}

function clearRegisterUserModalEdit() {
    document.getElementById("editusername").value = null;
    document.getElementById("editpassword").value = null;
}

function clearRegisterUserModal() {
    document.getElementById("registeruserid").value = null;
    document.getElementById("registerusername").value = null;
    document.getElementById("registeruserpassword").value = null;

}

function fillWorktimeModal() {
    document.getElementById("worktimeModalemployee_id").value = (getUser());
}