function generateUserlistChartBrut(payroll) {
    document.getElementById("userlistChartBrut").innerHTML = /*html*/`
        <div class="row justify-content-center">
            <div class="col-lg-10" style="text-align: center">
                <strong style="font-size: 28px">Töötajate töötunnid (kokku):</strong>
            </div>            
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <canvas id="userlistChartCanvasBrut2"></canvas>
            </div>
        </div>
    `;

    let ctx = document.getElementById('userlistChartCanvasBrut2').getContext('2d');
    let chartData = composeChartDataBrut(payroll);
    new Chart(ctx, {
        type: 'pie',
        data: chartData,
        options: {}
    });
}
