
function loadPayroll() {
    console.log("loadPayroll");
    fetchPayroll().then(generateUserlistList);
}

function loadEmployeePayroll() {
    console.log("loadEmployeePayroll");
    fetchPayroll().then(generateEmployeeUserlistList);
}
function loadCalendarMonths() {
    let id = document.getElementById("calendarId").value;
    let date = document.getElementById("calendarDate").value;
    clearError();
    fetchCalendarMonth(id, date);
    clearCalendarMonths();
}

function openUserlistModalForUpdate(id) {
    clearError();
    fetchUserlist(id).then(userlist => {
        openUserlistModal();
        clearUserlistModal();
        fillUserlistModal(userlist);
    });
}

function openRegisterUserModal() {
    openRegisterUserModalShow();
    clearError();
    clearRegisterUserModal();

}

function openRegisterUserModalEdit(id) {
    clearError();
    fetchUser(id)
        .then(user => {
            document.querySelector("#editid").value = user.id;
            openRegisterUserModalEditShow();
            clearRegisterUserModalEdit();
            fillRegisterUserModalEdit(user);
        });
}

function openWorktimeModalForInsert() {
    openWorktimeModal();
    clearErrorworktime();
    clearWorktimeModal();
    fillWorktimeModal();
}


function openUserlistModalForInsert() {
    openUserlistModal();
    clearError();
    clearUserlistModal();
}

function saveFile(event) {
    event.preventDefault();
    let fileInput = getFileInput();
    uploadFile(fileInput.files[0]).then(response => fillUserlistModalLogoField(response.url));
}

function saveUserlist() {
    let userlist = getUserlistFromModal();
    if (validateUserlistModal(userlist)) {
        postUserlist(userlist)
            .then(() => {
                closeUserlistModal();
                loadPayroll();
            });
    }
}

function saveRegisterUser() {
    let user = getRegisterUserFromModal();
    if (validateRegisterUser(user)) {
        registerUser(user)
            .then(() => {
                closeRegisterUserModalHide();
                loadPayroll();
            });
    }
}

function saveRegisterUserEdit() {
    let user = getRegisterUserFromModalEdit();
    if (validateRegisterUserEdit(user)) {
        saveUser(user)
            .then(() => {
                closeRegisterUserModalEditHide();
                loadPayroll();
            });
    }
}


function saveWorktime() {
    let calendar = getWorktimeFromModal();
    if (validateWorktimeFromModal(calendar)) {
        postWorktime(calendar)
            .then(() => {
                closeWorktimeModal();
                loadEmployeePayroll();
            });
    }

}
function deleteAllFromCalendarButton() {
    if (confirm('Soovid kõik kanded kustutada?')) {
        deleteAllFromCalendar().then(generateUserlistList);
        location.reload();
    }
}

function removeLastWorktimeEntry(id) {
    if (confirm('Soovid viimast kannet kustutada?')) {
        deleteWorktime(id).then(loadEmployeePayroll)
    }
}

function removeUser(id) {
    deleteUser(id).then(loadPayroll)
}

function removeUserlist(id) {
    if (confirm('Soovid kannet kustutada?')) {
        deleteUserlist(id).then(loadPayroll)
    }
}

function removeWorktime(id) {
    if (confirm('Soovid kannet kustutada?')) {
        deleteUserlist(id).then(loadPayroll)
    }
}

function loadChartBrut() {
    fetchPayroll().then(generateUserlistChartBrut)
}

function loadStatistics() {
    fetchPayroll().then(generateuserlistStatistic)
}

function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (validatEmployeeeCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            generateTopMenu();
            loadEmployeePayroll();
        })
    } else if (validateCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            generateTopMenu();
            loadPayroll();
        })
    }
}

function logoutUser() {    
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
    location.reload();
}