async function fetchPayroll() {
    try {
        let response = await fetch(
            `${API_URL}/payroll`,
            {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                }
            }
        );
        checkResponse(response);
        return await response.json();

    } catch (e) {
        return Promise.reject('Request failed' + e);

    }
}

async function fetchUser(id) {
    try {
        let response = await fetch(
            `${API_URL}/users/${id}`,
            {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                }
            }
        );
        checkResponse(response);
        return await response.json();

    } catch (e) {
        return alert('Kasutaja puudub. Palun loo töötajale kasutajakonto!')
            
    } 
}

function fetchCalendar() {
    return fetch(
        `${API_URL}/calendar`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
        .then(checkResponse).then(calendar => calendar.json());
}

function fetchCalendarMonth(employee_id, date) {
    return fetch(
        `${API_URL}/calendar/${employee_id}/${date}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
        .then(checkResponse).then(calendar => calendar.json());
}

function fetchCalendarData(employee_id) {
    return fetch(
        `${API_URL}/calendar/${employee_id}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
        .then(checkResponse).then(calendar => calendar.json());
}

function fetchUserlist(id) {
    return fetch(
        `${API_URL}/payroll/${id}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
        .then(checkResponse).then(userlist => userlist.json());
}

function uploadFile(file) {
    let formData = new FormData();
    formData.append("file", file);

    return fetch(
        `${API_URL}/files/upload`,
        {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            },
            body: formData
        }
    )
        .then(checkResponse).then(response => response.json());
}

function postUserlist(userlist) {
    return fetch(
        `${API_URL}/payroll`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(userlist)
        }
    )
        .then(checkResponse);
}

function postWorktime(calendar) {
    return fetch(
        `${API_URL}/calendar`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(calendar)
        }
    )
        .then(checkResponse);
}


function postUser(user) {
    return fetch(
        `${API_URL}/payroll`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(user)
        }
    )
        .then(checkResponse);
}

function deleteAllFromCalendar() {
    return fetch(
        `${API_URL}/calendar/delete`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
        .then(checkResponse);
}

function deleteWorktime(employee_id) {
    return fetch(
        `${API_URL}/calendar/${employee_id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
        .then(checkResponse);
}

function deleteUser(id) {
    return fetch(
        `${API_URL}/users/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
        .then(checkResponse);
}


function deleteUserlist(id) {
    return fetch(
        `${API_URL}/payroll/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
        .then(checkResponse);
}

function saveUser(user) {
    return fetch(
        `${API_URL}/users/update`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(user)
        }
    )
        .then(checkResponse)
    //.then(session => session.json());
}

function registerUser(credentials) {
    return fetch(
        `${API_URL}/users/register`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
        .then(checkResponse)
        .then(session => session.json());
}

function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
        .then(checkResponse)
        .then(session => session.json());
}

function checkResponse(response) {
    if (!response.ok) {
        clearAuthentication();
        showLoginContainer();
        closeUserlistModal();
        generateTopMenu();
        throw new Error(response.status);
    }
    showMainContainer();
    return response;
}