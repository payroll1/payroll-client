
function generateTopMenu() {
    let logoutLink = "";
    if (!isEmpty(getUsername())) {
        logoutLink = /*html*/`${getUsername()} | <a href="javascript:logoutUser()" style="color: #fff; font-weight: bold;">logi välja</a>`;
    }

    document.getElementById("topMenuContainer").innerHTML = /*html*/`
        <div style="padding: 5px; color: white; font-style: italic; background:black;">
            <div class="row">
                <div class="col-6">
                    <strong>Payroll 0.1 alpha</strong>
                </div>
                <div class="col-6" style="text-align: right; font-style: normal;">
                    ${logoutLink}
                </div>
            </div>
        </div>
    `;
}

function showLoginContainer() {
    document.getElementById("loginContainer").style.display = "block";
    document.getElementById("mainContainer").style.display = "none";
}

function showMainContainer() {
    document.getElementById("loginContainer").style.display = "none";
    document.getElementById("mainContainer").style.display = "block";
}

function changeView() {
    if (document.getElementById("viewSelect").value === "CHARTBRUT") {
        showUserlistChartBrut();
    } else if(document.getElementById("viewSelect").value ==="STATISTIC") {
        showEmployeeStatList();
    } else {
        if (getUsername()==='admin') {
            showUserlistList() 
        } else {
            showUserlistListForEmployee();
        }
    
    }
}
function showEmployeeStatList() {
    document.getElementById("userlistStatistic").style.display = "block";
    document.getElementById("userlistList").style.display = "none";
    document.getElementById("userlistChartBrut").style.display = "none";
    loadStatistics();
}

function showUserlistList() {
    document.getElementById("userlistStatistic").style.display = "none";
    document.getElementById("userlistList").style.display = "block";
    document.getElementById("userlistChartBrut").style.display = "none";
    loadPayroll();
}

function showUserlistListForEmployee() {
    document.getElementById("userlistStatistic").style.display = "none";
    document.getElementById("userlistList").style.display = "block";    
    document.getElementById("userlistChartBrut").style.display = "none";
    loadEmployeePayroll();    
}

function showUserlistChartBrut() {
    document.getElementById("userlistStatistic").style.display = "none";
    document.getElementById("userlistList").style.display = "none";    
    document.getElementById("userlistChartBrut").style.display = "block";
    loadChartBrut();
}
