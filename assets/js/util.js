
// Validation functions, see on SEE aken
// if (!isEmpty(userlist.dividends) && !isDecimal(userlist.dividends)) {
//     showError("Dividend peab olema number!");
//     return false;
//  (!isEmpty(calendar.workHours) && !isInteger(calendar.workHours) && calendar.workHours > 0) 
// }

function validateRegisterUserEdit(user) {
    if (isEmpty(user.username)){
        showErrorUserEdit("Kasutajanimi on kohustuslik");
        return false;
    } if (isEmpty(user.password)) {
        showErrorUserEdit("Parool on kohustuslik");
        return false;
    }
    return true;    
}

function validateRegisterUser(user) {
    if (isEmpty(user.id)){
        showErrorRegisterUser("Töötaja id on kohustuslik");
        return false;
    } if (isEmpty(user.username)){
        showErrorRegisterUser("Kasutajanimi on kohustuslik");
        return false;
    } if (isEmpty(user.password)) {
        showErrorRegisterUser("Parool on kohustuslik");
        return false;
    }
    return true;    
}

function validateWorktimeFromModal(calendar) {
    if (isEmpty(calendar.date)) {
        showErrorworktime("Kuupäev on kohustuslik!");
        return false;
    } if (isEmpty(calendar.workHours)) {
        showErrorworktime("Täida väli töötunnid!");
        return false;
    } if (!isInteger(calendar.workHours)) {
        showErrorworktime("Tunnitasu peab olema täisarv!");
        return false;
    } if (calendar.workHours >24) {
        showErrorworktime("Ei saa olla suurem kui 24!");
        return false;            
    } 
    return true;
} 

function validateUserlistModal(userlist) {
    if (isEmpty(userlist.name)) {
        showError("Nimi on kohustuslik!");
        return false;
    }
    if (!isEmpty(userlist.payPerHour) && !isInteger(userlist.payPerHour)) {
        showError("Tunnitasu peab olema täisarv!");
        return false;
    }
    if (!isEmpty(userlist.workHours) && !isDecimal(userlist.workHours)) {
        showError("Töötund peab olema number!");
        return false;
    }
    if (!isEmpty(userlist.gross_income) && !isDecimal(userlist.gross_income)) {
        showError("Brutopalk peab olema number!");
        return false;
    }    
    if (isEmpty(userlist.extra)) {
        showError("Väli on kohustuslik!")
        return false;
    }

    return true;
}

function validatEmployeeeCredentials(credentials) {
    return !(credentials.username === 'admin') && !isEmpty(credentials.password);
}

function validateCredentials(credentials) {
    return (credentials.username === 'admin') && !isEmpty(credentials.password);
}

function isInteger(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+$/;
    return regex.test(text);
}

function isDecimal(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+(\.\d+)?$/;
    return regex.test(text);
}

function isEmpty(text) {
    return (!text || 0 === text.length);
}

// DOM retrieval functions

function getFileInput() {
    return document.getElementById("file");
}

function getCalendarMonthFromInput() {
    return {
        "id": document.getElementById("calendarId").value,
        "date": document.getElementById("calendarDate").value,        
    };
}


function clearCalendarMonths() {
    return {
        "id": document.getElementById("calendarId").value = null,
        "date": document.getElementById("calendarDate").value = null,        
    };
}


function getWorktimeFromModal() {
    return {
        "date": document.getElementById("worktimeModaldate").value,
        "employee_id": document.getElementById("worktimeModalemployee_id").value,
        "workHours": document.getElementById("worktimeModalWorkHours").value,
    };
}

function getRegisterUserFromModalEdit() {
    return {
        "id": document.getElementById("editid").value,
        "username": document.getElementById("editusername").value,
        "password": document.getElementById("editpassword").value,
    };
}

function getRegisterUserFromModal() {
    return {
              "id": document.getElementById("registeruserid").value,
        "username": document.getElementById("registerusername").value,
        "password": document.getElementById("registeruserpassword").value,
    };
}

function getUserlistFromModal() {
    return {
        "id": document.getElementById("id").value,
        "name": document.getElementById("name").value,
        "logo": document.getElementById("logo").value,
        "birthDate": document.getElementById("birthDate").value,
        "payPerHour": document.getElementById("payPerHour").value,
        "workHours": document.getElementById("workHours").value,
        "gross_income": document.getElementById("gross_income").value,
        //"net_income": document.getElementById("net_income").value,
        "extra": document.getElementById("extra").value,
    };
}

function getCredentialsFromLoginContainer() {
    return {
        id: document.getElementById("id").value,
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}

// Financial functions

function formatNumber(num) {
    if (!isDecimal(num)) {
        return 0;
    }
    num = Math.round(num * 100) / 100;
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

// Chart Functions

function generateRandomColor() {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    return `rgb(${r},${g},${b})`;
}


function composeChartDatasetBrut(calendar) {
    if (calendar != null && calendar.length > 0) {
        let data = calendar.map(calendar => (calendar.workHours));
        let backgroundColors = calendar.map(generateRandomColor);
        return [{
            data: data,
            backgroundColor: backgroundColors,
        }];
    }
    return [];
}

function composeChartLabelsBrut(payroll) {
    return payroll != null && payroll.length > 0 ? payroll.map(userlist => userlist.name) : [];
}

function composeChartDataBrut(payroll) {
    return {
        datasets: composeChartDatasetBrut(payroll),
        labels: composeChartLabelsBrut(payroll)
    };
}